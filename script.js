let currentIndex = 0;
let autoplayInterval;
let posts;

async function slider() {
  try {
    const res = await fetch("https://jsonplaceholder.typicode.com/posts");
    if (!res.ok) {
      throw new Error("Error");
    }
    const data = await res.json();
    posts = data;
    showPost(currentIndex);
  } catch (error) {
    console.error(error);
    posts = [];
  }
}

async function showPost(index) {
  const sliderPosts = document.getElementById("slider-posts");

  if (index >= posts.length) {
    index = 0;
  }

  const post = posts[index];
  sliderPosts.innerHTML = `<h3>${post.title}</h3><p>${post.body}</p>`;
}

function prevPost() {
  currentIndex = (currentIndex - 1 + posts.length) % posts.length;
  showPost(currentIndex);
}

function nextPost() {
  currentIndex = (currentIndex + 1) % posts.length;
  showPost(currentIndex);
}

function randomPost() {
  const randomIndex = Math.floor(Math.random() * posts.length);
  currentIndex = randomIndex;
  showPost(currentIndex);
}

function startAutoplay() {
  autoplayInterval = setInterval(nextPost, 1000);
}

function stopAutoplay() {
  clearInterval(autoplayInterval);
  autoplayInterval = null;
}

function toggleAutoplay() {
  const autoplayBtn = document.getElementById("autoplay-btn");
  if (!autoplayInterval) {
    startAutoplay();
    autoplayBtn.innerText = "Stop Auto";
  } else {
    autoplayBtn.innerText = "Auto play";
    stopAutoplay();
  }
}

document.getElementById("prev-btn").addEventListener("click", prevPost);
document.getElementById("next-btn").addEventListener("click", nextPost);
document
  .getElementById("autoplay-btn")
  .addEventListener("click", toggleAutoplay);
document.getElementById("random-btn").addEventListener("click", randomPost);

slider();
